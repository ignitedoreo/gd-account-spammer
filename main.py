import random, requests
from string import ascii_letters, digits  # so we don't have to type [A-Za-z0-9] by hand
import base64, itertools
import hashlib
import base64
import time
import gzip

server = "http://sgdps2.ps.fhgdps.com/"

possible_letters = ascii_letters + digits

def randintstring(stringlength):
    return str(random.randint(10 ** (stringlength - 1), int("9" * stringlength)))

def base64_encode(string: str) -> str:
    return base64.urlsafe_b64encode(string.encode()).decode()

def base64_decode(string: str) -> str:
    return base64.urlsafe_b64decode(string.encode()).decode()

def generate_rs(n: int) -> str:
    return ("").join(random.choices(possible_letters, k=n))
def generate_uuid(parts: [int] = (8, 4, 4, 4, 10)) -> str:
    # apply generate_rs to each number in parts, then join results
    return ("-").join(map(generate_rs, parts))
def xor_cipher(string: str, key: str) -> str:
    result = ""
    for string_char, key_char in zip(string, itertools.cycle(key)):
        result += chr(ord(string_char) ^ ord(key_char))
    return result

def generate_upload_seed(data: str, chars: int = 50) -> str:
    # GD currently uses 50 characters for level upload seed
    if len(data) < chars:
        return data  # not enough data to generate
    step = len(data) // chars
    return data[::step][:chars]

def generate_chk(values: [int, str] = [], key: str = "", salt: str = "") -> str:
    values.append(salt)

    string = ("").join(map(str, values))  # assure "str" type and connect values

    hashed = hashlib.sha1(string.encode()).hexdigest()
    xored = xor_cipher(hashed, key)  # we discuss this one in encryption/xor
    final = base64.urlsafe_b64encode(xored.encode()).decode()

    return final

level_string = "H4sIAAAAAAAAC6WPwQ3DIAxFF3IlfxsDUU6ZIQN4gKzQ4Qt2j2mUKgf-E5j3BceuneDsMpaOBbiYOSShgeIveHUwszeHw2b0cb073n_oeKYvz3Q51SHmKdyoMJ_2r1dkSYjXNfWy5tZnyjNdT3Q6NijxhCVqotDIShjZ8uSLPrHrEjuJzIIYbCUyp-AEiFcQGhsJiRgpFVs_5qhX1YgCAAA="

proxylist = requests.get("https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list-raw.txt").text.split("\n")

n = 0
while True:
    n += 1
    us = randintstring(10)
    ps = randintstring(10)

    data = {
        "userName": us,
        "password": ps,
        "email": "a@a.a",
        "secret": "Wmfv3899gc9"
    }

    req = requests.post(server + "accounts/registerGJAccount.php", data=data)

    data = {
        "udid": generate_uuid(),
        "userName": us,
        "password": ps,
        "secret": "Wmfv3899gc9"
    }

    req = requests.post(server + "accounts/loginGJAccount.php", data=data)
    accid = req.text.split(",")[0]

    gjp = base64_encode(xor_cipher(str(ps), "37526"))

    data = {
        "accountID": accid, # PasswordFinders' Account ID
        "gjp": gjp, # This would be PasswordFinders' password encoded with GJP encryption
        "userName": us,
        "stars": 69696969,
        "demons": 69,
        "diamonds": 5000,
        "icon": 0,
        "color1": 21,
        "color2": 42,
        "iconType": 0,
        "coins": 150,
        "userCoins": 400,
        "special": 2,
        "accIcon": 0,
        "accShip": 0,
        "accBall": 0,
        "accBird": 0,
        "accDart": 0,
        "accRobot": 0,
        "accGlow": 0,
        "accSpider": 0,
        "accExplosion": 1,
        "secret": "Wmfd2893gb7",
        "seed": ''.join(random.sample("1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM", 10))
    }
    data['seed2'] = generate_chk([data['accountID'], data['userCoins'], data['demons'], data['stars'], data['coins'], data['iconType'], data['icon'], data['diamonds'], data['accIcon'], data['accShip'], data['accBall'], data['accBird'], data['accDart'], data['accRobot'], data['accGlow'], data['accSpider'], data['accExplosion']], "85271", "xI35fsAapCRg")

    r = requests.post(server + 'updateGJUserScore22.php', data=data)
    print(f"Account n{n} created ({accid})")

    for i in range(20):
        data = {
            "gameVersion": 21,
            "accountID": accid,
            "gjp": gjp,
            "userName": us,
            "levelID": 0,
            "levelName": f"{randintstring(10)}",
            "levelDesc": f"{base64.urlsafe_b64encode(randintstring(50).encode()).decode()}", # "A test level for the GD Docs!"
            "levelVersion": 1,
            "levelLength": 0,
            "audioTrack": 5, # This uses a newgrounds song
            "auto": 0,
            "password": 0,
            "original": 0,
            "twoPlayer": 0,
            "songID": 0, # NK - Jawbreaker
            "objects": 69,
            "coins": 0,
            "requestedStars": 69,
            "unlisted": 0, # This level is unlisted, but does exist!
            "ldm": 0,
            "levelString": level_string, # The level string for the level described above
            "seed2": generate_chk(key="41274", values=[generate_upload_seed(level_string)], salt="xI25fpAapCQg"), # This is talked about in the CHK encryption,
            "secret": "Wmfd2893gb7"
        }

        req = requests.post(server + "uploadGJLevel21.php", data=data)
        print(f"Level n{i + 1} on account {accid} uploaded ({req.text})")
        time.sleep(60)
